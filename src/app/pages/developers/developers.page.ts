import { Component, OnInit } from '@angular/core';
import { DatabaseService, Dev } from 'src/app/services/database.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-developers',
  templateUrl: './developers.page.html',
  styleUrls: ['./developers.page.scss'],
})
export class DevelopersPage implements OnInit {

  developers: Dev[] = [];
  products: Observable<any[]>;

  developer = {};
  product = {};

  selectedView = 'devs';

  constructor(private db: DatabaseService) { }

  ngOnInit() {
    this.db.getDatabaseState().subscribe(ready => {
      console.log(ready);
      if (ready) {
        this.db.getDevs().subscribe(devs => {
          console.log("Dev changed: " + devs);
          this.developers = devs;
        });
        this.products = this.db.getProducts();
      }
      console.log(this.developers);
      console.log(this.products);
    });
  }

  addDeveloper() {
    let skills = this.developer['skills'].split(',');
    skills = skills.map(skill => skill.trim());

    this.db.addDeveloper(this.developer['name'], skills, this.developer['img']).then(_ => {
      this.developer = {};
    });
  }

  addProduct() {
    this.db.addProduct(this.product['name'], this.product['creator']).then(_ => {
      this.product = {};
    })
  }
}
